'''
@file       getChange.py
@brief      This file contains the function 'getChange2'
@details    A function for determining the least change (least denominations) for a
            given payment and price.

@author     Ryan McLaughlin
@date       Originally created on 04/04/21 \n Last modified on 04/05/21
'''

def getChange( price , payment , debugFlag ):
    
    paid = 0
    
    for key in payment:   # 1) get payment in cents
        paid += key*payment[key]
    print('getChange: total paid = {:} cents'.format(paid))
    
    if paid >= price:                   # check for sufficient funds
    
        leftover = paid - price
        
        denoms = list(payment.keys())   # turn payment keys (denomination values) into list
        denoms.sort(reverse = True)     # sort list in descending order
        
        change = {}                     # define an empty dictionary
        for denom in denoms:           
            change[denom], leftover = divmod(leftover, denom)   # quotient, remainder = divmod( num , denom )
        
        
        leftoverAmount = 0  
        for key in change:              # get change in cents                               
            leftoverAmount += key*change[key]             
        print('getChange: total money left = {:} cents'.format(leftoverAmount))
        print('getChange: change is ' + str(change))        
        
    else:
        change = None
        print('getChange: Insufficient funds')
        
    return change



if __name__ == "__main__":
    
    payment1 = {1:3, 5:0, 10:0, 25:2, 100:1, 500:0, 1000:0, 2000:1}     # should be 2153 cents
    payment2 = {5:0, 1:3, 10:0, 25:2, 500:0, 100:1, 2000:1, 1000:0}     # same amount as payment 2, out of order
    price1 = 2000
    price2 = 3000
    
    myChange1 = getChange( price1 , payment1, True)      # test case w/sufficient funds
    print('')
    myChange2 = getChange( price2, payment1, True)      # test case w/insufficent funds
    print('')
    myChange3 = getChange( price1 , payment2, True)      # test case w/payment out of order
    print('')
    myChange4 = getChange( 10 , {1:0, 5:0, 10:0, 25:0, 100:0, 500:0, 1000:0, 2000:0} , True)