'''
@file       getChange1.py
@brief      This file contains the function 'getChange'
@details    A function for determining the least change (least denominations) for a
            given payment and price.

@author     Ryan McLaughlin
@date       Originally created on 04/04/21 \n Last modified on 04/04/21
'''

denom = {'p': 1, 'n': 5, 'd': 10, 'q': 25,
         'o': 100, 'f': 500, 't': 1000, 'w': 2000}
keysList = list(denom)                              # list of keys

def getChange1( price , payment ):
    
    paid = 0
    
    # 1) get total payment in cents, from denominations input
    for i in range(len(keysList)):                          # loop through all denominations
        paid += denom[keysList[i]]*payment[i]             # add each denominations * value to total leftover
    print('total paid = {:} cents'.format(paid))
    
    
    if paid >= price:   # sufficient funds
        change = [0]*len(keysList)
        
        # 2) determine change
        leftover = paid - price
        
        for i in range(len(keysList)-1,-1,-1):      # start with largest denomination
            denomValue = denom[keysList[i]]         # value of current denomination
            
            if leftover >= denomValue:              # if current denomination can be used in change
                change[i], leftover = divmod(leftover, denomValue)
                
        print('change is [' + ','.join(str(i) for i in change) + ']') 
        
        # check leftover amount in cents (check that this is working correctly)
        leftoverAmount = 0
        for i in range(len(keysList)):                          # loop through all denominations
            leftoverAmount += denom[keysList[i]]*change[i]      # add each denominations * value to total leftover
    
        print('total money left = {:} cents'.format(leftoverAmount))
                
    else:
        change = None
        print('Insufficient funds')
    
    return change



if __name__ == "__main__":
    
    payment = [3, 0, 0, 2, 1, 0, 0, 1]
    
    myChange = getChange1( 2000 , payment)      # test case w/sufficient funds
    print('')
    myChange2 = getChange1( 12000, payment)      # test case w/insufficent funds