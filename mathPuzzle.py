'''
@file       mathPuzzle.py
@brief      Program to determine solution to math puzzle from the book "Hacking: The Art of Exploitation,
            2nd Edition", by Jon Erickson
@details    \n *See source code here:* https://bitbucket.org/ryanmclaug/me_405/src/master/mathPuzzle.py \n

@author     Ryan McLaughlin
@date       Originally created on  04/09/21 \n Last modified on 04/19/21
'''

from itertools import permutations
from itertools import product
from time import monotonic_ns
from math import factorial

## @brief  Solver starting time
startT = monotonic_ns()     # timing mechanism

## @brief  Tuple of the four numbers that must be used (in string format)
num = ('1','3','4','6')         # available numbers

## @brief  Integer representing the total unique combinations of numbers
#  @details  For example: 1,3,4,6 is different than 6,4,1,3 from a solver standpoint
numLen = factorial(len(num))    # no repeats, use factorial

## @brief  Number of operations needed to construct a possible solution
opRepeat = 3

## @brief  Tuple of all possible operations
op = ('+','-','*','/','+(','-(','*(','/(',')+',')-',')*',')/')      # available arithmetic operations

##  @brief  Total number of combinations of three operations, with repeats allowed.
opLen = len(op)**opRepeat   # with repeats allowed

print('{:} possible combinations are available'.format(numLen*opLen))

## @brief  Initialize answer variable
ans = 0     # init ans variable

## @brief Counter variable for attempt number
counter = 1

## @brief  Intialize iterable for number sequences
numOptions = permutations(num)    # initialize iterator for numbers

if __name__ == "__main__":
    
    try:
        # for k in range(numLen):
        while ans != 24:
              
              ## @brief  tuple of number sequence from iterable `numOptions`
              nums = next(numOptions)                           # get sequence of numbers
              
              ## @brief  all possible operations, found using product() 
              opOptions = product(op, repeat = opRepeat)        # reinitialize options for arithmetic operators
              
              for _i in range(opLen):      # test all operator combinations
                  
                  ## @brief Tuple of operations from iterable `opOptions`
                  ops  = next(opOptions)    # get new operations sequence
                  
                  _j = 0     # index for string creation
                  ## @brief  Initialized string for solution
                  ansString = ''
                  
                  for _n in nums:
                      ansString += _n
                      
                      if _j+1 <= len(ops):   
                          ansString += ops[_j]
                          _j += 1
                  
                  ## @brief  Integer value representing the number of ) needed to form a valid expression
                  parenthDiff = ansString.count('(') - ansString.count(')')     # check parentheses
                  
                  if parenthDiff > 0:
                      ansString += ')'*parenthDiff      # add parentheses
                  
                  # print('answer is {:}'.format(str(ansString)))      # for debugging
                  
                  try:
                      ans = eval(ansString)     # use eval to evaluate string
                  except ZeroDivisionError:     # skip over divide by zero cases
                      ans = 0
                  except SyntaxError:
                      # print('syntax error occured')
                      ans = 0
                    
                  # print(ans)
                  if ans == 24:             # if solution is correct
                      print('success, answer = {:}'.format(ans))    
                      # print('solution is {:}'.format(ansString))
                      # ansList.append(ansString)
                      break

                  # print('value of answer is {:}'.format(ans))     # for debugging
                  
                  _j = 0         # reset counter for adding operators
                  counter += 1
        
        print('solution is {:}'.format(ansString))  
        print('solution found on attempt # {:}'.format(counter))
        print('\nelapsed time to solve = {:} s'.format( (monotonic_ns() - startT )/(10**9) ))
    
    except KeyboardInterrupt:
         print('Ctrl-c has been pressed, exiting program')
