'''
@file       vendotron.py
@brief      Program for interactive virtual vending machine
@details    This file contains code for a virtual vending machine. Its general functionality
            includes the following: \n
            - Insertion and reading of various denominations (penny up to $20 bill) at any time
            - Selection of a drink at any time
            - Ejection of leftover change at any time
            - User-oriented messages such as prices, options, etc. 
            - Function for determining least change for an input payment and price

            \n *See source code here:*  https://bitbucket.org/ryanmclaug/me_405/src/master/Lab0x01/vendotron.py \n
@image      html 405_Lab0x01_VendingMachineSTD.png "" width = 60% 

@author     Ryan McLaughlin
@date       Originally created on 04/08/21 \n Last modified on 04/13/21 
'''


import keyboard
import time

# keyboard callback function
def kb_cb(key):
    '''
    @brief      Callback method based on keyboard inputs
    @details    In order to use the keyboard to set certain tasks into motion, a callback is used, 
                along with the keyboard module for python. 
    @param      key is the key released by the user
    '''
    global lastKey
    lastKey = key.name


# zip takes two lists, associates elements in a row with each other


def getCents( moneyDict ):
    '''
    @brief      Function for determining monetary value of a set of denominations
    @details    This function utilizes the `.items()` method for dictionary objects,
                to loop through pairs of denomination values and quantities.
    @param      moneyDict is a dictionary containing the current set of denominations to be counted
    @return     cents is the value of all denominations in cents
    '''
    cents = sum(denomVal*denomQty for denomVal, denomQty in moneyDict.items())
    return cents

    
def getChange( price , payment , debugFlag ):
    '''
    @brief      Function for determining least change for a set of denominations
    @details    This function was first developed in HW0x01, and improved upon for use in the
                larger lab assigment of the vending machine. Given a set of denominations,
                the function must be able to determine the resulting change in the least number
                of denominations. If the payment is not sufficient, the function must recognize this
                and give a useful output.
    @param      price is the price in cents of the item to be purchased
    @param      payment is a dictionary of denominations and their respective quantities
    @param      debugFlag is a input to the overall generator set to True when debugging code
    @return     `change` can be one of two things: if the payment exceeds or matches the price,
                change is a dictionary contatining the least change, and if the payment is not
                enough for the given price, change is set to `None`.
    '''
    
    # 1) get cents paid
    paid = sum(denomVal*denomQty for denomVal, denomQty in payment.items())
    
    if debugFlag == True:
        print('getChange: total paid = {:} cents'.format(paid))
    
    if paid >= price:                   # check for sufficient funds
    
        leftover = paid - price
        
        denoms = list(payment.keys())   # turn payment keys (denomination values) into list
        denoms.sort(reverse = True)     # sort list in descending order
        
        change = {}                     # define an empty dictionary
        for denom in denoms:           
            change[denom], leftover = divmod(leftover, denom)   # quotient, remainder = divmod( num , denom )  
        
    else:
        change = None
        if debugFlag == True:
            print('getChange: Insufficient funds')
        
    return change


# generator function
def VendotronTask( debugFlag , idleTime):
    '''
    @brief      Generator function to run the main finite state machine
    @details    Each time the next() function is called for this function from within
                the "main script" of this file, the generator runs one iteration of the
                finite state machine, and yields the next state for the upcoming iteration.
    @param      debugFlag is used to relay more information to the console while in the
                program development phase
    @param      idleTime is the maximum allowable time without user action before a "scroll
                message" is displayed in the console
    @return     the varialbe `state` is yielded each time next() is called, and is the state corresponding
                to the starting point of the next iteration
    '''
    global lastKey
    
    # define dictionary to relate keys to denominations (for print statements and for dict ref)
    denoms = {'0':('penny',1) , '1':('nickel',5) , '2':('dime',10) , '3':('quarter',25) , 
             '4':('$1 bill',100) , '5':('$5 bill',500) , '6':('$10 bill',1000) ,
             '7':('twenty $ bill',2000)}
    
    # soda dictionary
    sodaOptions = {'c':('Cuke',100) , 'p':('Popsi',120) , 's':('Spryte',85) , 'd':('Dr. Pupper',110) }
    
    # dictionary for current balance
    balanceDict = {1:0, 5:0, 10:0, 25:0, 100:0, 500:0, 1000:0, 2000:0}      
    # integer for balance in cents
    balanceCents = 0                             
    
    # current drink price
    currentPrice = 0  
    
    # has a drink been selected?
    drinkSelected = False
    
    # has a new coin (or bill) been inserted?
    newCoin = False
    
    # does the user have sufficient funds for another drink
    anotherDrink = False
    
    # list for drinks available for next drink
    drinksAvailable = []
    
   
    zeroTime = time.monotonic()     # set zero time for scroll message

                                                
    S0_INIT                 = 0
    S1_WAITING_FOR_INPUT    = 1
    S2_DRINK_SELECTED       = 2
    S3_CHECK_BALANCE        = 3
    S4_EJECT_COINS          = 4
    S5_SCROLL_MESSAGE       = 5
    
    state = 0
    print('Welcome to Vendotron! \n')
    
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
        
        if debugFlag == True:
            print('state in is {:}'.format(state))
        
        if lastKey != None:         # read key press
            zeroTime = time.monotonic()     # reset time since action
            
            if lastKey in denoms:   # money inserted
                
                newCoin = True
                print('a {:} has been inserted'.format(denoms[lastKey][0]))
                balanceDict[denoms[lastKey][1]] += 1    # update balance
                balanceCents = getCents(balanceDict)    # re calculate cents
                    
                print('balance is ( ${:.2f} ) {:} '.format(balanceCents/100,str(balanceDict)))
            
            elif lastKey in sodaOptions:    # soda selected
                
                drinkSelected = True
                sodaString = sodaOptions[lastKey][0]
                print('You have selected a {:}'.format(sodaString))
                currentPrice = sodaOptions[lastKey][1]
                print('Current drink price is {:}'.format(str(currentPrice)))
                
            elif lastKey == 'e':        # eject
                print('ejecting change')
                state = S4_EJECT_COINS
                
            elif lastKey == 'q':        # quit
                print('quiting program')
                break
            
            lastKey = None
            
            
        timeSinceAction = time.monotonic() - zeroTime
        
        if state == S0_INIT:
            # display intro message
            print('To insert money, use the following keys: \n'
                  '0 = penny     1 = nickel    2 = dime \n'
                  '3 = quarter   4 = $1 bill   5 = $5 bill \n'
                  '6 = $10 bill  7 = $20 bill \n'
                  'Drink options: \n'
                  'c = Cuke   (100)   p = Popsi      (120) \n'
                  's = Spryte (85)    d = Dr. Pupper (110) \n')
            
            # always transition to S1
            state = S1_WAITING_FOR_INPUT
            
            
        elif state == S1_WAITING_FOR_INPUT:
            timeSinceAction = time.monotonic() - zeroTime
            
            # option 1: if timeSinceAction > idleTime go to state 5
            if timeSinceAction >= idleTime:
                state = S5_SCROLL_MESSAGE            
            
            # option 2: if drinkSelected == True, transition to s2
            elif drinkSelected == True:
                state = S2_DRINK_SELECTED

            
        elif state == S2_DRINK_SELECTED:
            # option 1: new coin has been inserted, or user 
            # has been prompted for another drink, go to state 3 to check balance
            if newCoin == True or anotherDrink == True:
                anotherDrink = False
                newCoin = False
                state = S3_CHECK_BALANCE
            
            # option 2: newCoin == False, go back to state 1
            elif newCoin == False:
                state = S1_WAITING_FOR_INPUT


        elif state == S3_CHECK_BALANCE:
            
            change = getChange(currentPrice,balanceDict, debugFlag)
            
            # option 1: balance < currentPrice, go back to state 1 and display message
            if change == None:
                # dont change balanceDict, no money taken out
                print('insufficent funds')
                state = S1_WAITING_FOR_INPUT
            
            # option 2: balance > currentPrice, go to state 4
            else:
                drinkSelected = False
                balanceDict = change
                balanceCents = getCents(balanceDict)
                print('----vending a {:}----'.format(sodaString))
                print('balance is ( ${:.2f} ) {:} '.format(balanceCents/100,str(balanceDict)))
                   
                if balanceCents > 84:   # at least the price of cheapest drink
                    drinksAvailable = []
                    
                    for idx in list(sodaOptions.keys()):  # which drinks user has enough money for
                        if balanceCents >= sodaOptions[idx][1]:
                            drinksAvailable.append(sodaOptions[idx][0])     # add strings to list
                    
                    if len(drinksAvailable) > 0:
                        anotherDrink = True
                        print('\nanother beverage?\n'
                              'enough money for a {:} \nor press e to eject change'.format(', '.join(drinksAvailable) ) )

                else:
                    print('\nremaining balance is insufficient for another drink,\n'
                          'insert more money or press e to eject change')
                
                state = S1_WAITING_FOR_INPUT
                
        
        elif state == S4_EJECT_COINS:
            
            # always give change here
            print('change returned is ( ${:.2f} ) {:}\n'.format(balanceCents/100,str(balanceDict)))
            
            # reset machine
            balanceDict = {1:0, 5:0, 10:0, 25:0, 100:0, 500:0, 1000:0, 2000:0}      
            balanceCents = 0                             
            currentPrice = 0  
            drinkSelected = False
            newCoin = False
            anotherDrink = False

                
            # always transition back to state 1
            state = S0_INIT
            
            
        elif state == S5_SCROLL_MESSAGE:
            # display scroll message
            # print('{:} seconds have elapsed without action, show scroll message'.format(idleTime))
            print('\n----Insert money or select a drink----\n')
            zeroTime = time.monotonic()
            state = S0_INIT
            
        
        
        if debugFlag == True:
            print('state out is {:}'.format(state))
        
        yield(state)


        

if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object
    
    ## @brief Instance of generator/task
    #  @details Although this program does not utilize a class structure, it does use an object
    #  based structure. In this case, vendo is an instance of the function/generator VendtronTask
    #  which contains the main finite state machine for the file.
    vendo = VendotronTask(debugFlag = False, idleTime = 15 )
    
    ## @brief List containing all keys used as callbacks
    callbackKeys = ['0','1','2','3','4','5','6','7','e','c','p','s','d','q']
    

    
    for _thisKey in callbackKeys:  
        keyboard.on_release_key(_thisKey, callback = kb_cb)
    
    ## @brief The last key released on the keyboard (must be a member of the list `callbackKeys`)
    lastKey = None
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            next(vendo)
            
    except KeyboardInterrupt:
        print('Ctrl-c has been pressed, exiting program')
        keyboard.unhook_all()
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')
        keyboard.unhook_all()
    