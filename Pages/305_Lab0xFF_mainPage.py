## @file 305_Lab0xFF_mainPage.py
#  @brief Documenation page for ME 305, Lab 0xFF
#
##  @page 305_lab0xFF 305 Lab 0xFF: Final Project
#  
#  In the final four weeks of ME 305: Introduction to Mechatronics, we were tasked with learning
#  to interact with quadrature encoders, DC motors, and controller drivers to create a full system
#  design. Additionally, serial communication was introduced as a means of sharing information between
#  our Nucleo microcontroller board and our PC running a Python script. Overall, this was a challenging
#  project that provided a great opportunity to learn new hardware, as well as see the full benefits
#  of creating classes to run multiple instances of a certain object. \n\n 
#  Click on a week below to learn more about the project tasks for each week.
#
#  - @subpage week1_305
#  - @subpage week2_305
#  - @subpage week3_305
#  - @subpage week4_305 \n
# 
#
#
#
## @page week1_305 Week 1: Starting a User Interface (UI) and Sending Arbitrary Data
#  
#  In the first week of the final project, our main goal was to learn how to send data via serial.
#  Previously, having only a Nucleo developement board, we were only able to use one USB port to send/recieve
#  data. However, with the new hardware arriving, our new setup included two USB ports: one on the board itself,
#  and another connected directly to the board MCU that we refered to as the "native" USB port. This allowed for
#  seperation of debugging in the Putty window from user interface print statements in Python, an invaluable
#  upgrade to our previous workflow. The following sections below outline in more detail some of the files
#  created in week 1.
#  
#  @section frontEnd Front End: User Interface Running in Python
#  To begin this lab, we needed to write a script to send and recieve data to and from the Nucleo board. Simplicity
#  was a main key in the end for this script, as unlike some of the other code we have written, a finite state machine 
#  structure is not neccesarily needed. For full documentation of this file, see frontEnd.py. \n\n
#  *Note: All documented files are final versions.*
#
#  @section uiTask  UI Task: Complement to Front End Running on Nucleo
#  To go along with the aforementioned script running in Python, we need a task running on the microcontroller
#  board to facilitate serial communication. As other tasks such as encoders, motors, and controllers (discussed
#  in more detail in weeks 2-4) are running, the UI Task runs at a faster period, and is in control of data collection,
#  sending data back to the PC, and translating commands sent from the PC. Since there are many different phases of
#  system operation (initialization, collecting data, transmitting data, etc.), a finite state machine has been
#  implemented for this file, and the corresponding state transition diagram is shown below. For full documentation
#  of the final version of the UI Task, see uiTask.py.
#
#  @image      html Lab0xFF_uiTask_week1.jpg " " width = 50%
#  @details    \n <CENTER> **Figure F.1.** UI task state transition diagram for week 1. </CENTER> \n\n
#
#  @section deliverables1 Week 1 Deliverables
#  Lastly, our week 1 deliverable was to sample and collect data from an arbitrary decaying sinewave function
#  on the Nucleo, send it to the PC through serial, and finally plot the data using `matplotlib` on the PC side.
#  This final plot is shown below in Figure F.2, and is a preview into the larger picture goal of creating
#  plots based on encoder data, not just a predefined function.
#
#  @image      html decayFunction.png " " width = 40%
#  @details    \n <CENTER> **Figure F.2.** Plot generated in Python, based on data collected on the Nucleo. </CENTER> \n\n
#
#  Click here to advance to \ref week2_305.
#  
#
#
## @page week2_305 Week 2: Interfacing with Encoders
#  In week 2, with serial communication mostly figured out, it was time to add in hardware. The first component to our
#  system were encoders. More specifically, our developement board has two E4T optical encoders, one for each motor (see
#  \ref week3_305 for more detail on motors). Although the code developed in week 2 was straightforward, major difficulties arose
#  due to hardware issues. For reasons never fully understood, the encoders we were using would at times cap out at a certain
#  value, and would sporatically work again. These issues continued somewhat into the coming weeks, but again for reasons
#  unknown, the encoders seemed to function more consistently in the follwoing weeks when being run simultaneously with a
#  controller task.
#
#  @section encDriver Encoder Driver
#  Before adding in powered motors, we began with understanding how to read and correctly update the position of our encoders. 
#  The main tasks to be completed in week 2 were: setting up the encoders on the correct timer channels, and implementing an 
#  algorithm to translate changes in timer counts into angular displacement values for the encoders. This algorithm is needed
#  because while encoder angle in either the positive or negative direction is unbounded, the encoder count has a limit of
#  65535 ticks.\n\n In addition to this algrorithm, methods for retrieving the current position, current delta value (difference
#  between the last two positions), and current speed were all added to the encoder driver. These would not only be called on
#  by certain commands from the PC (when the user presses a given keyboard key), but these methods would be used by the controller
#  task developed in weeks 3 and 4. For full documentation of the encoder driver class, see encoder.encoder. See below for the 
#  state transition diagram for the encoder task (later replaced by a more complete controller task, see \ref controllerTask).
#
#  @image      html Lab0xFF_encoderTask.jpg " " width = 35%
#  @details    \n <CENTER> **Figure F.3.** Encoder task state transition diagram developed in week 2. </CENTER> \n\n
#  
#  @section encTask Encoder Task
#  Although we could run the encoder driver itself directly in a main file, the best way to seperate a driver class from the corresponding
#  task is to create a new file. As this file was absorbed into a larger controller task file (see controllerTask.py), the source code for
#  the encoder task only is not available, but the main premise was to run methods from the encoder drivers periodically. With this task
#  setup, we can easily create multiple objects of encoders. For a task diagram from week 2, see Figure F.4 below, and for the final task
#  diagram, see \ref finalTaskDiagram.
#
#  @image      html Lab0xFF_taskDiagram_week2.jpg " " width = 35%
#  @details    \n <CENTER> **Figure F.4.** System task diagram developed in week 2. </CENTER> \n\n
#
#  @section sharedData Sharing Data Between Tasks
#  In addition to drivers and a task for the encoders, we now needed a method for sharing data between files on the Nucleo.
#  The most efficient way of sharing data between files is to used a shared file, which all files on the board have access to.
#  For example, if a PC command (key press) is sent to the Nucleo, the UI Task can add a new command to a queue in the shared file
#  (see shares.py). This command can then be read by the encoder or controller task, causing some action. Using this sequence of events,
#  the PC and all files on the Nucleo can interact with each other, instead of having to recreate data in multiple places. 
#  
#  @section uiUpdates Changes to the UI Task Finite State Machine
#  Lastly, the UI task finite state machine underwent some small changes from week 1 to week 2. With encoders gathering data (and more of
#  it than before with just a function), we only wanted to collect data for a maximum of 15 seconds. The state transition diagram shown
#  below for the UI task was not altered in any significant way after week 2.
#
#  @image      html Lab0xFF_uiTask_week2.jpg " " width = 50%
#  @details    \n <CENTER> **Figure F.5.** UI task state transition diagram for weeks 2-4. </CENTER> \n\n
#
#  Click here to advance to \ref week3_305.
#
#
#
## @page week3_305 Week 3: Interfacing with DC Motors and Implemenation of a Controller
#  With encoders now accurately reading the position of each motor, it was time to add in motors that could be controlled either
#  from the PC, or according to pre-defined setpoint values. Week 3 also saw the introduction of a controller driver and task, which
#  marked the final critical step in developing a integrated system. See the preceding sections for more detail on the files created
#  and deliverables related to Week 3
# 
#  @section motorDriver Motor Driver
#  The first of two drivers added in week 3, the motor driver class was developed to create multiple instances of motors to be controlled
#  from a controller task (see controllerTask). Similar to the encoder driver class, the motor driver class was setup such that any number
#  of motors could be used by changing the input parameters (timer, channels, etc.). With the motor driver, an even more simplistic algorithm
#  was employed to convert a desired duty cycle value (between -100% and 100%) into the appropriate PWM. This algorithm went into the main method
#  for the class, `set_duty`, which would be used by the controller task to update motor effort level during operation. For full documentation of
#  the motor driver class, see motorDriver.motorDriver.
#  
#  @section controllerDriver Controller Driver
#  With drivers developed for both the encoders and motors, the last step was developement of a contorller driver to read the encoders, and update
#  the motors accordingly. To start, a closed loop P controller (P for proportional) was used, but this was soon replaced by a PI controller, which
#  combines proportional and integral function into a single closed loop controller. As is shown in the tuning plots from week 3 (see deliverables3),
#  the main issue with a P controller for speed control is that as the error between desired and acutal speed approaches zero, so will the controller
#  output PWM level. Because of this, a P controller can never settle to exactly the desired value, it can only come close. To combat this issue,
#  we can instead use a PI controller based on position and speed (I standing for integration/integral). 
#
#  \n\n Using equations derived in lecture, an initial guess for K<SUB>p</SUB> was determined (see Figure F.6 below for full derivation). 
#  Due to time constraints, K<SUB>i</SUB> was picked solely based on testing.
#
#  @image      html Kp.jpg " " width = 60%
#  @details    \n <CENTER> **Figure F.6.** Derivation of K<SUB>p</SUB>. </CENTER> \n\n
#  
#  For full documenation of the controller driver class, see closedLoop.closedLoop.
#
#  @section controllerTask Controller Task
#  In order to implement a controller, a controller task to run the encoders, motors, and controllers was needed. This file replaced the earlier
#  encoder task, and was in control of 6 different objects (2 encoders, 2 motors, and 2 controllers). Each time a period has elapsed (set from within
#  the main script running all tasks), the workflow of the controller task is the following: \n
#  \n   > Get position and speed values from encoders (using methods defined with their respective driver classes)
#  \n   > Interpolate desired position and speed values from resampled reference data
#  \n   > Determine the new PWM level for each motor using the controller objects
#  \n   > Set motors based on controller output\n\n
#  Using this sequence, we can achieve setpoint or reference tracking, depending on the reference data set. See Figure F.7 below for the state
#  transition diagram of the controller task, and see controllerTask.py for full file documentation.
#
#  @image      html controllerTask_STD.jpg " " width = 30%
#  @details    \n <CENTER> **Figure F.7.** Controller task state transition diagram developed in week 3. </CENTER> \n\n
# 
#  Note: No changes were made to the controller task finite state machine between weeks 3 and 4, therefore this diagram is only show in week 3.
#
#  @section deliverables3 Week 3 Deliverables
#  In week 3, our main goal was to determine a K<SUB>p</SUB> value to give us the best system response. Shown below is a progression of tuning plots
#  for the determination of K<SUB>p</SUB>. Note that figure numbers generated directly from Python do not reflect trial numbers, and were only used
#  to differentiate between motors/encoders.
#
#  @image      html week3_1.png " " width = 40%
#  @details    <CENTER> **Figure F.8.** Initial tuning plot, with K<SUB>p</SUB> set too high. </CENTER> \n\n
#
#  With K<SUB>p</SUB> set too high, all corrections made to the PWM level were too significant to level out at a given setpoint value. Next, I tried
#  decreasing K<SUB>p</SUB>.
#
#  @image      html week3_2.png " " width = 40%
#  @details    <CENTER> **Figure F.9.** Decreasing K<SUB>p</SUB>. </CENTER> \n\n
#
#  By decreasing K<SUB>p</SUB>, I was able to decrease the fluctuation dramatically from the initial run, but there was still too much oscillation 
#  to be functional.
#
#  @image      html week3_3.png " " width = 40%
#  @details    <CENTER> **Figure F.10.** Decreasing K<SUB>p</SUB> below ideal value. </CENTER> \n\n
#
#  Again decreasing K<SUB>p</SUB>, this trial resulted in too low of a value for K<SUB>p</SUB>. The final plot below was taken at a value between
#  those used in Figures F.9 and F.10.
#
#  @image      html week3_4.png " " width = 40%
#  @details    <CENTER> **Figure F.11.** Best trial. </CENTER> \n\n
#  From this tuning, my final value was K<SUB>p</SUB> = 0.05 min/rev. This value would be carried into testing of the final system, but changed
#  slightly with the introduction of
#  PI control. \n\n
#
#  Click here to advance to \ref week4_305.
#  
#
#
## @page week4_305 Week 4: Switching from Setpoint Tracking to Refrence Tracking
#
#  In the last week of our final projet, no new files/classes were added. Instead, it was time to modify our code for reference tracking 
#  (following a path of predefined, changing speed/position) based on a reference csv. However, given an oversampled data set, we first 
#  needed to resamle the data, load it onto the Nucleo, and finally create a method for interpolating from the reference data. A more in-depth
#  description of changes to files previously created, as well as the final refrence tracking plot, can be found below.
#  
#  @section frontEndChanges Updates to the Front End (PC script)
#  The first main addition to frontEnd.py was adding a resampling method, `sampleCSV`, which takes in the sample data, and creates a new
#  csv file, resampled at a given rate. For example, the original data set used for this project was at a rate of 0.001 seconds (1 ms) 
#  per data point, whereas the resampled csv had a resolution of .025 s (25 ms). As long as the resampled CSV has a resolution similar to
#  that of the controller period, we will not run into any data issues. The reason for this resampling is mainly for memory concerns on the
#  Nucleo. \n\n In addition to resampling data, the refrence data was plotted together with data collected from encoders to provide a
#  clear visual interpretation of how succesfully the system is working. And lastly, the values used in a given trial for K<SUB>p</SUB>,
#  K<SUB>i</SUB>, and J (our performance metric) were sent through serial to be added as text inside the plot area. To see the final source
#  code for the UI task, see uiTask.py.
#
#  @section controllerTaskChanges Updates to the Controller Task
#  Some minor changes/additions were made to the controller task, starting with the calculation of J, a performance metric. Each time the
#  controller task updated encoders/motors, the summation portion of J was updated accordingly in the controller task (and in the shared
#  data file shares.py). Then, when the UI sent J to the Front End for displaying on the final plot, the adjusted J value was found by dividing
#  this summation value by the total number of data points taken. \n\n In addition to calculating J, a short method was added for loading data from a 
#  CSV file (on the board) into arrays in the controller task file. To see the final source code for the controller task, see controllerTask.py.
# 
#  @section finalTaskDiagram Final Task Diagram
#  With many variables being shared between files, a task diagram is useful to describe task interactions. See Figure F.12 below for the
#  final system task diagram.
#
#  @image      html Lab0xFF_taskDiagram.jpg " " width = 50%
#  @details    <CENTER> **Figure F.12.** System task diagram. </CENTER> \n\n
#
#  @section finalResults Final Results
#  To conclude documentation of this project, the best trial run of the integrated system developed for this project has been shown below. Although
#  J was our agree upon metric for measuring performance, this metric is also not balanced in regards to how errors in position or speed affect the
#  final value, so I based my criteria for "best result" based partially on J, and partially on the visual check of how similar the position
#  data matched the given reference position data.
# 
#  @image      html BestPlot.png " " width = 40%
#  @details    <CENTER> **Figure F.13.** Best system plot, with K<SUB>p</SUB>, K<SUB>i</SUB>, and J shown for reference. </CENTER> \n\n
#
#  Return to \ref index.  


