## @file 305_Lab0x03_mainPage.py
#  @brief Documenation page for ME 305, Lab 0x03
#
##  @page 305_lab0x03 305 Lab 0x03: Simon Says
#  
#  Due to a delay in neccesary hardware being sent out to students, the third lab
#  of the quarter was limited once again to a button and an LED. For the most part,
#  this lab was mainly an exercise in writing more complex code in Python. The goal
#  of the lab was to create a game similar to the popular childrens game "Simon Says".
#  In our spinoff "Nucleo Says", the objective of the game was to match LED morse code
#  sequences with button presses. On the code development side, the main challenge of
#  this lab was to interpret the user button presses as a dot, dash, element space, or
#  letter space. Since a human cannot hold a button down for an exact amount of time
#  some tolerance needed to be built in, and accounted for in interpreting various
#  button press signals. In addition to this button interpretation, a user-friendly
#  interface was also a key aspect to this lab. Lastly, this lab was the first in
#  which a class was implemented, as the final lab would be nearly impossible without
#  the modular abilities of classes. For full documentation of the class created
#  for this lab, see game.py.
#
#
#  Click here for the final lab, \ref 305_lab0xFF.