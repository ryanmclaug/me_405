## @file 405_Lab0x02_mainPage.py
#  @brief Documenation page for ME 405, Lab 0x02
#
##  @page 405_lab0x02 405 Lab 0x02: Reaction Times
#  
#  *Code documentation:* 
#  - Using old methods (software based), Lab0x02_A.py (source code: 
#    https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0x02/Lab0x02_A.py)\n
#  - Using new methods (hardware based), Lab0x02_B.py (source code: 
#    https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/405_Lab0x02/Lab0x02_B.py)
#   
#  \section partA Part A: Software Based Methodology
#  In ME 305, timers and interupt service routines were used frequently. For Part A
#  of this lab, our task was to use the `utime` module (specifically the methods of
#  `utime.ticks_us` and `utime.ticks_diff`) to calculate a user reaction time to an
#  LED flashing. To ensure the user could not predict when the LED would flash,
#  the LED was setup to flash on for 1 second at a time, with a random wait time between
#  2 and 3 seconds between turning on. This randomization was achieved using the 
#  `random.randrange()` method of the `random` module. Addtionaly, a simple state
#  machine was utilized, and the corresponding state transition diagram can be found
#  below in Figure 2.1.\n\n
# 
#  @image      html Lab0x02A_MainFSM.jpg  " " width = 30%
#  @details    <CENTER> **Figure 2.1.** State transition diagram for Part A of Lab 0x02. </CENTER> \n\n
#  
#  For the interupt callback, the blue user button was setup as an external interupt,
#  which when called could set a boolean variable to True (signifying to the main script)
#  that the button had been pressed and a reaction time should be calculated. Although this
#  code gives a reasonably accurate picture of reaction time, for more advanced physical
#  systems such as those we will work with later in the quarter, timing is essential. A
#  controller cannot effectively do its job if the latency of the system is too high, so
#  we need to figure out a more precise/fast way to track the timing of events.\n\n
#
#  \section partB Part B: Hardware Based Methodology
#  In Part B of this lab, we moved from the naive way of doing things with code only to
#  more effectively using hardware components accesible on the Nucleo Developement Board.
#  Specifically, two imporant concepts were put into practice:\n\n
#
#  1. **Output Compare:** With output compare, a timer channel is set to toggle a specific pin
#  (the LED in our case) whenever a specified output compare value is reached on the count.
#  In terms of code implementation, this requires the timer channel mode to be set to
#  `mode = pyb.timer.OC_TOGGLE`, as well as a callback to be associated with the compare
#  value being reached. For the ouptut capture callback, a brief state machine was used to
#  differentiate between setting the LED to toggle on after a random time between 2 and 3 seconds,
#  versus setting the LED to turn off after 1 second. In each case, the required interval for the
#  next compare value is added to the previous OC value. See Figure 2.2 below for the associated state
#  transition diagram for the OC interupt.\n\n
#
#  @image      html OC_FSM.jpg  " " width = 25%
#  @details    <CENTER> **Figure 2.2.** State transition diagram for the Output Compare callback. </CENTER> \n\n  
#  
#  2. **Input Capture:** In conjunction with Output Compare, a second interupt related to the user
#  button was setup using Input Capture. With Input Capture, the count value is stored
#  each time the button was pressed. However, since the button is not itself connected to a timer,
#  a jumper wire was connected between PC13 (the button) and PB3 (connection to TIM2). This allows
#  for a falling edge on the button (setup in the timer channel as `polarity = timer.FALLING`) to
#  trigger an Input Capture callback.\n\n
#  
#  With the Output Compare value of when the LED first turns on, and the Input Capture of when
#  a falling edge is detecting on the button, a reaction time can be easily calculated. Furthermore,
#  a list was appended each time a reaction was calculated, to give the user an average reaction time
#  during and after the program is run. For the state transition diagram associated with the main
#  code of Part B, see Figure 2.3 below.
# 
#  @image      html Lab0x02B_MainFSM.jpg  " " width = 20%
#  @details    <CENTER> **Figure 2.3.** State transition diagram for the main code of Part B of Lab 0x02. </CENTER> \n\n
#  
#  \section lab2_demoVideos Demonstration Videos
#  Video links for demonstration videos are provided below. These document the 
#  working Python scripts. \n
#  - Please see the demo video for Assignment A below: \n
#    https://youtu.be/wwxmhQ7PBL0 \n
#  - Please see the demo video for Assignment B below: \n
#    https://youtu.be/XeTE6-wlpoI \n\n
# 
#  \section lab2_discussionQuestions Discussion Questions
#  *1. Which method is better for this type of application?*\n
#  For tracking timing of events occuring relatively quickly, such as reaction time
#  or controlling a precision physical system (such as the Balancing Platform Term Project),
#  the method used in Part B is superior. By using hardware based methods, transitions from
#  one part of code to another are eliminated from the calculation of timing. For example,
#  with the method of Part A, the time for the program to go from turning on the LED, as well
#  as the time between recognizing an interupt has been triggered and actually capturing this time
#  are including in the overall reaction time. This makes for an inaccurate measurement in the case
#  of this lab, and a potentially faulty system in a broader context.\n\n
#  
#  *2. Which method would be prefered for a different time scale?*\n
#  Although the method of Part B is ideally suited for short periods of time between 
#  events, it has its downfalls. If the time between events (such as a button being pressed)
#  is greater than the timer period, it becomes much more difficult to process the count value.
#  Keeping track of such overflow would require using a counter for the number of times an overflow
#  occured, and at this point it is likely that the resolution available with hardware would be
#  unnecesary. For these reasons, it is always important to consider the time scale when
#  choosing between hardware or software timing methods.
#
#  Click here for the next lab, \ref 405_lab0x03.

