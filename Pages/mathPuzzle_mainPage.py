## @file mathPuzzle_mainPage.py
#  @brief Documenation page for math puzzle solution
#
##  @page mathPuzzle
#
#  Source file: mathPuzzle.py 
#
#  This program came about from a math puzzle given to me by a friend. The problem
#  comes from the book "Hacking: The Art of Exploitation, 2nd Edition", by Jon
#  Erickson. The basic premise is this: with the numbers 1, 3, 4, and 6 as well as
#  the four basic math operations  (add, subtract, multiply, divide), how
#  can you get 24? Each number can only (and must be) used only once, and
#  operations can be used in any order/repetition. \n\n
#
#  After being unable to solve the problem by hand (or in other words, without
#  the help of a computer), I was intrigued by the idea of attempting to write a
#  program to do so. The idea of the solver is that for any given solution, there
#  will be some combination of the four numbers (1, 3, 4, or 6), and some combination
#  of three operations (+, -, *, /). For example, 1*3+4/6 is a potential solution.\n\n
#
#  However, this set of solutions is much too simplistic, and cannot possibly form
#  the correct solution. When doing math in our heads, we do not think explicitly
#  placing parentheses in front of or after numbers (most of the time), because
#  we can either think through the operations in a certain order, or write out an
#  expression in a form a computer cannot. Therefore, there are eight additional
#  operations, ( +(, ..., )+, ... ) where ... represents the other three operations
#  with similar parentheses placement. This allows for something like 4*(3-1)+6 to
#  be checked as a solution as well. \n\n
#
#  Next, it is one thing to have an idea, but to implement it in code then takes
#  another level of understanding. The premise of my solution method was based on
#  a brute force method, meaning check every possible combination available until
#  a solution is found. For this, I needed a few tools from the itertools library
#  (see documentation <a class="custom" 
#  href="https://docs.python.org/3/library/itertools.html" 
#  target="_blank" rel="noopener noreferrer">here</a>).
#  With the four numbers, only unique combinations needed to be checked, so an
#  iterable was generated using the method `permutations`. With the operations,
#  repeats are allowed so all combinations needed to be checked, giving rise to
#  the use of the `product` method for Cartesian Product. Although both are interesting
#  discussions on their own, I will not go into the details here. \n\n
#  
#  Finally, this program makes use of the built-in `eval()` method. In showing
#  this program to other Python users, I was suprised to find that many were not
#  aware that `eval()` can evaluate a string as a mathematical expression. For example,
#  the line eval('4+2') would give an output of 6. Utilizing this, I was able
#  to build mathematical expressions using strings in a loop structure (looping through
#  both sequences of numbers and combinations of operations), and then evaluate each
#  expression using `eval()`.\n\n
#  
#  Overall, writing this solver was a new experience that taught me more about
#  working with strings, and more generally gave me a new perspective on ways I
#  could utilize Python. In writing code, creativity and cleverness is very
#  important, and this was a fun and challenging exercise in both of those skills.\n\n
#
#  Click here to return to \ref index.  