## @file 405_Lab0x01_mainPage.py
#  @brief Documenation page for ME 405, Lab 0x01
#
##  @page 405_lab0x01 405 Lab 0x01: Vendotron
#  
#  *Code Documentation:* 
#  - Vending Machine Simulator (vendotron.py):
#    <a class="custom" href="https://bitbucket.org/ryanmclaug/me_405/src/master/Lab0x01/vendotron.py" 
#    target="_blank" rel="noopener noreferrer">Vendotron Source Code</a> \n\n
# 
#  The first project of ME 405 consisted mainly of Python review and the introduction of 
#  generators. In the past, finite state machine implementation was based on a "run" method
#  within a class. However, using a generator and the `next()` method of a generator, we can
#  achieve a more straightforward finite state machine structure. \n\n
#  In terms of the actual program functionality, as is described in more detail as a part of
#  the file documentation (see vendotron.py), this program simulates a simple vending machine.
#  It has four different drink choices, the ability to add money through various denominations,
#  and a simple user-interface in the Spyder console. Some of the main learning outcomes from
#  this lab were:
#  - Reviewing the use of keyboard keys (numbers for adding money and letters for drink 
#    selection/other options) as interupts
#  - Working with dictionaries and the various available methods for working with them
#  - Learning to effectively use a generator and the `next()` method \n
#
#  To demonstrate how a user would interact with this program, see the annotated image below for
#  one example of user interaction: \n
#
#  @image      html 405_Lab0x01_Console.png  " " width = 65%
#  @details    <CENTER> **Figure 1.1.** Example console readout for putting in money, buying two drinks
#              and getting money back through ejection interupt. </CENTER> \n\n
#
#  Note that this is only one simple example, and that many other possible paths of operation
#  are available.\n\n
# 
#  Click here for the next lab, \ref 405_lab0x02.
