## @file 305_Lab0x01_mainPage.py
#  @brief Documenation page for ME 305, Lab 0x01
#
##  @page 305_lab0x01 305 Lab 0x01: Fibonacci Calculator
#  
#
#  In the first week of writing code for ME 305, we were tasked with creating a Fibonacci number calculator
#  for any given (valid) index value. The Fibonacci sequence is a mathematical pattern in which the next value
#  is the sum of the last two values. Since this is a recursive pattern, it needs a root or base to go off of.
#  Therefore, the zeroth and first Fibonacci numbers are 0 and 1. Accordingly, the second Fibonacci number 
#  is 0 + 1 = 1, the third is 1 + 1 = 2, and so on. \n\n As this was our first real program written in Python,
#  the goal of this lab was to become familiar with recursion, learn how to implement a function, and review
#  simple logic. See Figure 1.1 below for an example of displayed messages from this program, or see
#  Fibonacci.py for the full file documentation. \n
#
#  @image      html Lab0x01_example.jpg " " width = 40%
#  @details    <CENTER> **Figure 1.1.** Example output in Spyder console based on two valid inputs (integers),
#              and one invalid input (a character). </CENTER> \n\n
#
#  Click here for the next lab, \ref 305_lab0x02