## @file mainPage.py
#
##  @mainpage Table of Contents
#  
#  @section me305 ME 305: Introduction to Mechatronics
#  As the first course for Mechatronics concentration students, ME 305 is an introduction
#  to microcontrollers and object-oriented programming in Python. Interupts, timers, and 
#  pulse-width modulation are introduced, and then implemented in a final project that 
#  combines motors, encoders, and a controller to create an integrated system and user 
#  interface. For more detailed documentation of each lab, see the links below.\n\n
#  
#  Week 2:       \ref 305_lab0x01 \n
#  Weeks 3-4:    \ref 305_lab0x02 \n
#  Weeks 5-6:    \ref 305_lab0x03 \n
#  Weeks 7-10:   \ref 305_lab0xFF \n\n
#
#  @section me405 ME 405: Mechatronics
#  The follow-up to ME 305, ME 405 builds on the basic concepts of its predecessor.
#  Topics covered include microprocessor applications, applied electronics, drive
#  technology, electromechanical systems, real-time programming, and mechatronic
#  design methodology. The final lab project consisted of balancing (and locating)
#  a ball on a platform. For more detailed documentation of each lab, see the 
#  links below.\n\n
#
#  Week 2-3:    \ref 405_lab0x01 \n
#  Week 4:      \ref 405_lab0x02 \n
#  Week 5:      \ref 405_lab0x03 \n
#  Week 6:      \ref 405_lab0x04 \n
#  Weeks 7-10:   \ref 405_lab0xFF \n\n
#  
#  @section addtlCode Additional Code Examples
#  Although most of my efforts in coding go into class projects, occasionaly a problem
#  or idea comes about that sparks my curiosity and leads to wrting code. The following
#  programs are some of the more interesting instances of such code. \n\n
#  
#  Math puzzle solution:  \ref mathPuzzle \n\n\n
#
#  <em>HTML Stylesheet credited to <a class="custom" href="https://jothepro.github.io/doxygen-awesome-css/index.html" 
#  target="_blank" rel="noopener noreferrer">Doxygen Awesome</a>, originally created by jothepro</em>



