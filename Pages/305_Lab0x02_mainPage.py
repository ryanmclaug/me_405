## @file 305_Lab0x02_mainPage.py
#  @brief Documenation page for ME 305, Lab 0x02
#
##  @page 305_lab0x02 305 Lab 0x02: Getting Started With Hardware
#  
#
#  Now familiar with basic Python syntax and logic, the second lab introduced two simple
#  hardware components, a button and an LED (both located directly on the Nucleo 
#  microcontroller board. Using a callback interupt, our program needed to allow a user
#  to cycle through various LED patterns (square, sine, and sawtooth waves). Topics
#  covered in relation to this lab included pulse-width modulation (for LED control),
#  writing non-blocking code, and working with interupts. For full documentation of the
#  related file, see ledpwm.py.\n\n
#
#
#  Click here for the next lab, \ref 305_lab0x03.